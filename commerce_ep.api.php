<?php

/**
 * Alter the data that is sent to banklink form generation.
 *
 * @param $settings
 * @param $payment
 * @param $payment_method
 * @param $order
 */
function hook_commerce_ep_payment_redirect_settings_alter(&$settings, &$payment, &$payment_method, $order) {
  // Do some sltering
}
